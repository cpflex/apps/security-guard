/**
 *  Name:  app.p
 *
 *  This module is the proprietary property of Codepoint Technologies
 *  Copyright (C) 2019 Codepoint Technologies
 *  All Rights Reserved
 */

//System includes.
#include "system.i"
#include "ui.i"
#include "motion.i"
#include "power.i"
#include "telemetry.i"
#include "log.i"
#include "datastore.i"

//Application includes.
#include "OcmNidDefinitions.i"
#include "MotionTracking.i"
#include "Battery.i"
#include "emergency.i"
#include "LocationConfig.i"
#include "TelemMgr.i"
#include "NvmRecTools.i"

 #include "version.i" //Compile time generated file.
 #include "cpflexver.i"

/*******************************************************
* Forward Declarations.
*******************************************************/
forward bool: SendStatus( );
forward bool: operatingMode_ButtonHandler( idButton, ButtonPress: press, ctClicks);
forward _ProcessSyslogCommand( value  );
forward  ResultCode: _ReportSyslogStatus( seqOut);
#pragma warning disable 213

/*******************************************************
* System events.
*******************************************************/
@SysInit()
{
	#ifdef __SYSLOG__
	//	//Test mode we automatically enable the System Log.
		SysSetLogFilters( 0xFF, 0xFF);
	#endif 

	Log(   "******** Security Guard Tracker App ***********",	MC_info, MP_med, LO_default);
	LogFmt("** Version:      %s",								MC_info, MP_med, LO_default, __VERSION__);
	LogFmt("** CP-Flex Ver:  %s",								MC_info, MP_med, LO_default, CPFLEX_VERSION);
	LogFmt("** Build Date:   %s %s",							MC_info, MP_med, LO_default, __DATE__, __TIME__);
	Log(   "***********************************************",	MC_info, MP_med, LO_default);
	
	//Get current power state
	new pctCharged;
	new PowerEventType: powerStatus;
	PowerCurrentStatus(pctCharged, powerStatus);
	trk_Init();
	trk_ReportZeroMeasurements(true); 
	loccfg_Init();
	e911_Init();
	battery_Init();
	TelemMgr_Init();
}

/*******************************************************
* Handle UI Events.
*******************************************************/
@UiButtonEvent( idButton, ButtonPress: press, ctClicks)
{
	TraceDebugFmt( "Button: id=%d, press=%d, clicks=%d", idButton, press, ctClicks);
	new bool: bHandled;
	
	if( loccfg_IsConfigModeActive() ) {
		bHandled = loccfg_OnButtonEvent(idButton, press, ctClicks);
	} else {
		bHandled = operatingMode_ButtonHandler(idButton, press, ctClicks);
	}

	//Signal user if failed.
	if( !bHandled ) UiSetLed( LED2, LM_count, 3, 150, 100 );
}

#pragma warning disable 203
/**
* @brief Power state change has occured.
*/
stock app_EnterPowerSaving(pctCharged)
{
	// Tracking is deactivated to preserve battery.	
	TraceDebug("Entering App Power Saving Mode")
	trk_EnterPowerSaving();
	battery_SendBatteryInfo();
}

stock app_ExitPowerSaving(pctCharged)
{
	TraceDebug("Exiting App Power Saving Mode")
	trk_ExitPowerSaving();
	battery_SendBatteryInfo();	
}
#pragma warning enable 203


/*******************************************************
* Operating Mode UI Control
*******************************************************/
bool: operatingMode_ButtonHandler( idButton, ButtonPress: press, ctClicks)
{
	new bHandled = false;

	switch(press)
	{
		case BP_short: 		{	bHandled = battery_IndicateChargeLevel(); }
		//case BP_double:		{   bHandled = TelemMgr_IndicateNetworkStatus();  }
		case BP_long:		{	bHandled = e911_SetMode(true);}
		case BP_verylong:	{	bHandled = e911_SetMode(true);}
		case BP_multi:      {   
			if( ctClicks >= 3 && ctClicks <= 5 ) {
				bHandled = true;
				bHandled = e911_SetMode(false);
			
			} else if( ctClicks == 8 && idButton == BTN2) {
				bHandled = true;
				loccfg_ToggleConfigMode();
			}
		}
	}
	
	return bHandled;
}

/*******************************************************
* Implementation helpers.
*******************************************************/
stock ReportTemperature( seq)
{
	//Convert tracking status to OCM spec.
	new int:temp = MotionGetTemperature();
	TraceDebugFmt("Device Temp: %d Celsius", MotionGetTemperature());

	//Send an Int32 Message
	return TelemSendInt32Msg( seq, NID_PedCmdsTemperature,temp, 1 );	
}

stock bool: SendStatus( )
{
	new ResultCode: rc;
	new Sequence:seqOut;
	new bool: bHandled = rc == RC_success;

	rc = TelemSendSeq_Begin( seqOut, 100); 
	if( rc != RC_success)
		return false;
	
	rc |= trk_ReportStatus(seqOut);
	rc |= battery_ReportStatus( seqOut); 
	rc |= battery_ReportChargeLevel( seqOut); 
	rc |= ReportTemperature(seqOut);

	TelemSendSeq_End( seqOut, !bHandled);

	if( bHandled)
		UiSetLed( LED1, LM_count, 3, 150, 100 );

	return  bHandled
}

/*******************************************************
* Communication Implementation
*******************************************************/

/**
* @brief Received data sequence handler.
* @remarks.  Processes received sequences and reports any requested. data.
*/
@TelemRecvSeq( Sequence: seqIn, ctMsgs )
{
	//Process the received sequence.
	new Message: msg;
	new MsgType: tmsg;
	new tcode;
	new id;
	new EventPriority: priority;
	new EventType: etype;
	new ResultCode: rc;
	new ct= 0;
	new Sequence:seqOut;

	//Allocate a sequence to send messages.
	//Create a sequence to hold all potential commands.
	rc = TelemSendSeq_Begin( seqOut, 150); 
	if( rc != RC_success)
	{
		TraceError("Could not allocate sequence buffer, ");
	}
	
	//Process each of the received messages.
	while( rc == RC_success 
		&& TelemRecv_NextMsg( seqIn, msg, tmsg, tcode, id, priority, etype) 
			== RC_success)
	{
		//Process simple messages first. These are commands to respond with 
		//some report.
		if( tmsg == MT_Int32Msg)
		{
			switch( id) 
			{
			case NID_PedCmdsTracking:	 {rc = trk_ProcessCommand(seqOut, msg); ++ct;  }
			case NID_PedCmdsConfig: 	 {rc = trk_ReportSettings(seqOut); ++ct;  }
			case NID_PedCmdsTemperature: {rc = ReportTemperature( seqOut); ++ct;  }
			case NID_PedCmdsBattery: 	 {rc = battery_ReportStatus( seqOut); ++ct;  }
			case NID_PedCmdsBatterylevel: {rc = battery_ReportChargeLevel( seqOut); ++ct;  }
			case NID_PedCmdsEmergency: {rc = e911_ReceiveMsg( seqOut, msg); ++ct; }
			case NID_ArchiveSyslog: { 
					new cmdvalue;
					//Set the System Log Archive Filter if defined.
					if( RC_success == TelemRecv_ToInt32Msg( msg,cmdvalue) ) {
						_ProcessSyslogCommand( cmdvalue);
					}
					rc =  _ReportSyslogStatus( seqOut);
					++ct;

				}
			case NID_ArchiveErase: {
					ArchiveErase(false);
				}
			}
		}
		//Next check to see if we received a settings message.
		else if( tmsg == MT_KeyValueMsg && id == NID_PedCmdsConfig )
		{
			trk_UpdateSettings(msg);
		}
	}

	//Done with the input sequence.
	TelemRecv_SeqEnd( seqIn);

	//Either send or discard output sequence 
	//send only if there are messages to send and we were successful in creating them.
	TelemSendSeq_End( seqOut, rc != RC_success || ct == 0);
	if( rc != RC_success)
		TraceError("Cannot send sequence");
}

static _ProcessSyslogCommand( value  ) 
{

	new archive;
	switch(value) {
		case NID_ArchiveSyslogDisable: archive = 0x0;
		case NID_ArchiveSyslogAlert: archive =  0x30;
		case NID_ArchiveSyslogInfo:	archive = 0x40;
		case NID_ArchiveSyslogDetail: archive = 0x50;
		case NID_ArchiveSyslogAll: archive = 0x60;
	}

	SysSetLogFilters( archive, 0xFF);
}

static ResultCode: _ReportSyslogStatus( seqOut) 
{
	new archive, terminal, telemetry, cmdvalue;
	SysGetLogFilters( archive, terminal, telemetry);

	switch(archive & 0xF0) 
	{
		case MC_unspecified: cmdvalue = NID_ArchiveSyslogDisable;
		case MC_alert: cmdvalue = NID_ArchiveSyslogAlert;
		case MC_info: cmdvalue = NID_ArchiveSyslogInfo;
		case MC_detail: cmdvalue = NID_ArchiveSyslogDetail;
		case MC_debug: cmdvalue = NID_ArchiveSyslogAll;
	}
					
	return TelemSendInt32Msg( seqOut, NID_ArchiveSyslog, cmdvalue);
}