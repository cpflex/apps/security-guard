#  Security Guard Application
This Flex application provides basic functions useful for security guard and employee safety 
applications in a multi building campus environment.   This application works best with the CT1000 (Nali N100) tracking tag and latest firmware.

**Version: 1.0.6**<br/>
**Release Date: 2023/09/19**

## Key Features  
The tracker application is designed to provide useful tracking and event 
information as an individual goes about their daily activities.
Key features include:

1. Emergency Alert Mode (user and cloud managed)
2. Motion activated location data collection
3. BLE based positioning only
4. Cloud configurable tracking settings / report
5. Cloud controlled tracking activation / deactivation
6. Battery status events and report
7. Temperature Monitoring
8. Location measurement configuration control for admin
9. Caching of messages when out of range 
10. Out of Network Caching (NEW)
11. BLE Beacon filtering based on beacon names (future function)
12. ~6000 locates with fully charged battery

## Settings ##

Some or all the settings may be changed by downloading configuration messages to the device. The configuration
values are stored in non-volatile memory and will retain their settings between resets.
### Tracking Settings

Default reporting intervals are one (1) minute for normal mode and one (1) minute for emergency mode. Motion trigger threshold is 200mg.

### Network Link Check Settings
Network link Checks are performed using a combination of time and activity metrics.  Uplink checks are performed
in accordance to the following schedule.  If confirmed messages are sent, the schedule is updated.

|  Link Check Metric                 |      Value            |
|:-----------------------------------|:---------------------:|
|   Stationary Time Interval         |   every 720 minutes   |
|   Stationary Uplink Activity       |   every 10 messages   |
|   Active Tracking Time Interval    |   every 10 minutes    |
|   Active Tracking Uplink Activity  |   every 3 messages    |

These settings can be adjusted by implementing a custom ConfigLinkCheck.i

### Out of Network (OON) Caching Settings
The firmware now supports OON caching of messages, if the link check fails messages marked with the TPF_ARCHIVE
flag, will be stored until the metric becomes available.   Once the network is available at DR3 or higher, the
device will push three messages per minute until the archive is empty.  This is in addition to any 
messages stored in the uplink queue, which may contain up to 5 messages.

# User Interface 

## General Operating Mode
General operating mode is entered when the device boots. The tracking mode is on and 
the tag reports location in coarse mode every one(1) minute.  

*Note:  If the tag is on the charger when booting, the entry to general operating mode will be delayed
2 minutes*

**General Operating Mode Button Descriptions**

The following table summarizes the user interface for the application.  See the sections
below for more detailed information regarding each button action.
 
| **Action**                        |     **Button**     | **Description**                               |
|-----------------------------------|:------------------:|-----------------------------------------------|
| Long press and hold (> 1.5 seconds) | any button or both | Activates emergency mode (see Emergency Mode) |
| 3 to 5 quick presses              | any button or both | Deactivates emergency mode                    |
| single, quick press               | any button or both | Battery status (see Battery Indicator below)  |
| > 15 sec. press and hold          | any button or both | Reset device and network settings             |
| > 25 sec. press and hold          | any button or both | Reset device to factory settings              |

**LED Descriptions** 

The following table indicates the meaning of the two bi-color LEDs on the device.  The LED's are
positioned:

* **LED #1** - Upper LED, furthest away from charger pads
* **LED #2** - Lower LED, closest to charger pads

| **Indication**                           | **LEDs** | **Description**                              |
|------------------------------------------|:--------:|----------------------------------------------|
| Flashing Red for three (3) sec.          | LED #1   | Emergency mode activated                     |
| Red blink every five (5) sec.            | LED #1   | Emergency mode is active                     |
| Flashing Green for three (3) sec.        | LED #1   | Emergency mode deactivated.                  |
| Green blink three times                  |   both   | Device reset                                 |
| Orange blink every ten (10) sec.         |  LED #2  | Battery is charging                          |
| Green blink every Ten(10) sec.           |  LED #2  | Battery is fully charged                     |
| Two (2) Red blinks every two (2) minutes | LED #2   | Battery is nearly dead, place tag on charger |
| Three (3) Red quick blinks               | Both     | Invalid Input                                |

### Battery Indicator 

Quickly pressing any button will indicate the percent charge of the battery in 20% increments.

LED #2 will blink as follows to indicate percent charge.

| LED #2   | % Charge    | Description|
|----------|-------------|------------|
|  1 red   |   < 5%      | Battery level is critical,  charge immediately. |
|  1 green |  5%  - 30%  | Battery low  less than 30% remaining. |
|  2 green |  30% - 50%  | Battery is about 40% charged. |
|  3 green |  50% - 70%  | Battery is about 60% charged. |
|  4 green |  70% - 90%  | Battery  is about 80% charged. |
|  5 green |  > 90%      | Battery is fully charged. |

Device enters power saving mode at 5%, which disables tracking. Tracking will be re-enabled when battery is charged more than 10%.  
## Emergency Mode 

Emergency mode provides a panic button feature, when the user holds either or both buttons for more than
three (3) seconds, the emergency mode is triggered.  When activated, it sends an emergency activated message to 
the cloud and begins tracking the user.  A locate is requested immediately when emergency is activated. The tag scans for BLE beacons every minute when when there is significant motion. 

The specified emergency tracking reporting interval is 
one (1) minute by default but can be modified by the cloud services.

The emergency mode can be deactivated by the user. Quickly click any button for five (5) times. The emergency mode will 
toggle off. The emergency function can also be deactivated by the cloud service.

## Tracking Mode ##

In normal tracking mode, the tag will start reporting location at regular intervals while there is significant motion
within the reporting interval.  If the device stops moving significantly for an entire reporting interval, location 
reporting is suspended until the unit is once again moving. The default reporting interval is one (1) minute. The interval
can be adjusted by the cloud services.

## Network status ##
When the tag leaves network coverage area, it will try to connect to the network when there is a message needs 
to be uploaded to the cloud. The tag will save its scanning data when it is out of the network. The saved location data 
will be send to the cloud when the tag comes back to the network area. If the tag is not moving, it will check the 
network status according to loRaWAN standard.


### Out of network (OON) / Out of Coverage area ###
When the tag moves out of LoRaWAN network coverage area, it will store at least 2000 location messages in the memory.
When the tag comes back to the network coverage area, it will upload stored messages based on customer requirements. The upload
function will be defined based on customer application.

When the device is out of the network coverage area, it will slow its attempts to reconnect to the network over time to save power. 

RETRY SCHEDULE

| Interval |        **Uplink Offset**       | **Comment**                                                          |
|:--------:|:------------------------------:|----------------------------------------------------------------------|
|  **10**  |   < tLastConfirmedUplink + 60  | Attempt every FLEX_TELEM_DEFAULT_MINUPLINK_SEC seconds for 1 minute. |
|  **120** |  < tLastConfirmedUplink + 1800 | Attempt every 2 minutes for 30 minutes.                              |
|  **600** | < tLastConfirmedUplink + 36000 | Attempt every 10 minutes for 10 hours.                               |
| **1200** | < tLastConfirmedUplink + 86400 | Attempt every 20 minutes for 24 hours.                               |


REJOIN SCHEDULE (Retries first before rejoin)

| **Interval (sec)** |   **state**   | **Time Period (sec)**    | **Comment**                                |
|:------------------:|:-------------:|--------------------------|--------------------------------------------|
|        **0**       | > unavailable | ctCache = 0              | No data pending.                           |
|       **10**       | > unavailable | ctCache > 1              | Minimum uplink interval.                   |
|       **120**      | > disabled    |                          | Cycles no action                           |
|       **30**       | > disabled    | < tUnavailable + 60      | Attempt 2 times for first 60 seconds.      |
|      **1800**      | > disabled    | < tUnavailable + 3600    | Attempt every 30 minutes up to an hour.    |
|      **4200**      | > disabled    | < tUnavailable + 36000   | Attempt every 1.25  hours up to 10  hours. |
|      **28800**     | > disabled    | < tUnavailable + 86,400  | Attempt every 8  hours up to 24 hours.     |
|      **43200**     | > disabled    | < tUnavailable + 604,800 | Attempt every 12  hours up to 1 week.      |
|      **86400**     | > disabled    | > tUnavailable + 604,800 | Attempt every 24 hours until battery dies. |

## Settings ##

App version 1.6+ supports non-volatile downlink settings for standard/emergency reporting intervals and accelerometer parameters.

  These values survive reboot or dead battery conditions.


## Source Code ##

The source code for the pedestrian tracker is found in the `src` folder and is organized 
into the following module files:
   * **app.p/app.i** -- main application system events, communications, and UI control.
   * **battery.p/battery.i** -- Battery and power management functions
   * **emergency.p/emergency/i** -- Emergency management functions
   * **tracking.p/tracking.i** -- Tracking controller.
   * **PedTrackerBasic.ocm.json** -- OCM file defines commands and data structures supported by the device.  CP-Flex translates data from the device into these structures to present to the cloud application.
   * **OCMNidDefinitions.i** -- OCM Code Generated NID definitions for CP-Flex communications.   Derived from the PedTrackerBasic.ocm.json
   

---
*Copyright 2022 Codepoint Technologies, Inc.* 
*All Rights Reserved*

