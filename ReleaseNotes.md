﻿# Security Guard Application Release Notes


### V1.0.6.0 - 230919
1. Firmware Reference v2.3.0.0
   * Incorporates all stabilization improvements.  This is a nearly final release candidate for the firmware.
2. Flex Platform: v1.4.0.2
3. Standard EU band ADR enabled.

### 1.0.5.0, 230502
1. Firmware reference v2.1.1.1a
   * Changed I2C memory bus from 100Khz to 400Khz.
   * Fixed accelerometer and i2c memory bugs.
   * Changed BVT LED behavior (manufacturing).
2. Platform version v1.3.1.4

### V1.0.4.0 230417
1. Updated Firmware Bundle to V2.0.15.0a
   - Reworked positioning control to ensure OPL is shutdown to conserve power.
   - Includes updates from alpha bundle v2.0.14.0a.
  
### V1.0.3.0 230125 ###
1. References updated firmware bundle to v2.0.10.3
   - Adds production support for temperature
2. References platform SDK version v1.2.13.0
   - Fixes Downlink (health ping) polling interval initialization.

### V1.0.2.1 230118 ###
- Build v1.0.2.1 230118
    1)  References updated firmware bundle to v2.0.9.1
        * Fixes hibernation power drain issue.
- Build v1.0.2.0 230203
    1)  References updated firmware bundle to v2.0.9.0
        * Fixes foundation bugs.

### V1.0.1.0 221227 ###
1.  References updated v2.0.8.4 firmware bundle supporting manufacturing BVT bugfixes.
2.  Uses V1 Library v1.2.11.1, which increases motion tracker default sensitivity, now 50mg.

### V1.0.0.0 221207 ###
*v1.0.0.1*
1) Changed UI patterns.
   i) Emergency mode now requires long or very long press.
   ii) Deactivating emergency mode required between 3 and 5 clicks.
*v1.0.0.0*
1)  Initial Implementation
    i)  Implemented with library version v1.2.10.0
    ii) Firmware version: v2.0.7.5
---
*Copyright 2022, Codepoint Technologies, Inc.* 
*All Rights Reserved*
