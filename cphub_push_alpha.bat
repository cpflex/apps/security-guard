@ECHO OFF
if [%1]==[] goto usage

set specfile="hub_spec.json"
set tag=%1
ECHO Pushing Security Guard Application version %1
cphub push -v -s %specfile% cpflexapp "./security_guard.bin" nali/applications/security-guard:%tag%

goto :eof
:usage
@echo Usage: %0 ^<tag (e.g. v1.0.2.3)^>
exit /B 1
